﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace TestApp.Shapes
{
    

    public class CircleShape : Shape
    {
        public double CenterX { get; set; }
        public double CenterY { get; set; }
        public double Radius { get; set; }

        public override void Transform(Transform transform)
        {
            if (transform is TranslateTransform)
            {
                var translation = transform as TranslateTransform;
                CenterX += translation.OffsetX;
                CenterY += translation.OffsetY;
            }
            else if(transform is RotateTransform)
            {
                Console.WriteLine("You spin me, right round baby, right round...");
                Console.WriteLine("Like a record baby, right round round round...");
                Console.WriteLine("Rotation of a circle is pointless");
                Console.WriteLine("In the same plane at least...");
            }
        }
    }

    public class TriangleShape : Shape
    {
        public double X1 { get; set; }
        public double X2 { get; set; }
        public double X3 { get; set; }

        public double Y1 { get; set; }
        public double Y2 { get; set; }
        public double Y3 { get; set; }

        public override void Transform(Transform transform)
        {
            if(transform is TranslateTransform)
            {
                var translation = transform as TranslateTransform;
                X1 += translation.GetOffsetSum();
                X2 += translation.GetOffsetSum();
                X3 += translation.GetOffsetSum();
                Y1 += translation.GetOffsetSum();
                Y2 += translation.GetOffsetSum();
                Y3 += translation.GetOffsetSum();
            }
            else if(transform is RotateTransform)
            {
                var rotation = transform as RotateTransform;
                var r1 = rotation.RotatePoint(X1, Y1);
                var r2 = rotation.RotatePoint(X2, Y2);
                var r3 = rotation.RotatePoint(X3, Y3);

                X1 = r1.Item1;
                X2 = r2.Item1;
                X3 = r3.Item1;
                Y1 = r1.Item2;
                Y2 = r2.Item2;
                Y3 = r3.Item2;
            }
        }
    }

    public class RectangleShape : Shape
    {
        public double Top { get; set; }
        public double Left { get; set; }
        public double Bottom { get; set; }
        public double Right { get; set; }

        public override void Transform(Transform transform)
        {
            if (transform is TranslateTransform)
            {
                var translation = transform as TranslateTransform;
                Top += translation.GetOffsetSum();
                Left += translation.GetOffsetSum();
                Right += translation.GetOffsetSum();
                Bottom += translation.GetOffsetSum();
            }
            else if (transform is RotateTransform)
            {
                
            }
        }
    }

    public class ShapeWrapper
    {
        public List<Shape> Shapes { get; set; }

        public static ShapeConverter GetConverter()
        {
            return new ShapeConverter();
        }
    }

    public class ShapeConverter : CustomConverter<Shape>
    {
        public override Shape ReadJson(JsonReader reader, Type objectType, Shape existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            var jObject = JObject.Load(reader);
            var discriminator = DynamicMapper.GetValueFromKey(nameof(JsonEntity.Type), jObject);
            switch (discriminator)
            {
                case "circle":
                    return DynamicMapper.CreateFromJObject<CircleShape>(jObject);
                case "rectangle":
                    return DynamicMapper.CreateFromJObject<RectangleShape>(jObject);
                case "triangle":
                    return DynamicMapper.CreateFromJObject<TriangleShape>(jObject);
                default:
                    throw new NotSupportedException("No support for such shape");
            }
        }

        public override void WriteJson(JsonWriter writer, Shape value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
