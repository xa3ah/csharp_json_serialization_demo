﻿using System.Collections.Generic;
using System.Linq;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var shapes = IO.DeserializeShapes().Shapes.ToList();
            shapes.ForEach(x => x.PrintProperties());
        }
    }
}
