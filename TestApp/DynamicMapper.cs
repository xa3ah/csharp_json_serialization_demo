﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace TestApp
{
    public static class DynamicMapper
    {
        public static T CreateFromJObject<T>(JObject jObject) where T : new()
        {
            var resultObject = new T();
            var properties = typeof(T).GetProperties();

            foreach(var prop in properties)
            {
				var propertyValue = GetValueFromKey(prop.Name, jObject);
				SetObjectValue(prop, resultObject, propertyValue);
            }


			return resultObject;
        }

		public static string GetValueFromKey(string key, JObject jObject)
		{
			key = Char.ToLowerInvariant(key[0]) + key.Substring(1); // toCamelCase
			return jObject[key].Value<string>();
		}

		private static void SetObjectValue(PropertyInfo propertyInfo, object target, string value)
		{
			var propertyType =
				Nullable.GetUnderlyingType(propertyInfo.PropertyType) ??
				propertyInfo.PropertyType;

			if (propertyType == typeof(Guid))
			{
				var parsedValue = Guid.Parse(value);
				propertyInfo.SetValue(target, parsedValue);
			}
			else if (propertyType == typeof(String))
			{
				propertyInfo.SetValue(target, value);
			}
			else if (propertyType == typeof(DateTime))
			{
				var parsedValue = DateTime.Parse(value);
				propertyInfo.SetValue(target, parsedValue);
			}
			else if (propertyType == typeof(Boolean))
			{
				var parsedValue = Boolean.Parse(value);
				propertyInfo.SetValue(target, parsedValue);
			}
			else if (propertyType.IsEnum)
			{
				var parsedValue = Enum.Parse(propertyType, value);
				propertyInfo.SetValue(target, parsedValue);
			}
			else if (propertyType == typeof(Int32))
			{
				var parsedValue = Int32.Parse(value);
				propertyInfo.SetValue(target, parsedValue);
			}
			else if (propertyType == typeof(Int64))
			{
				var parsedValue = Int64.Parse(value);
				propertyInfo.SetValue(target, parsedValue);
			}
			else if (propertyType == typeof(Single))
			{
				var parsedValue = Single.Parse(value);
				propertyInfo.SetValue(target, parsedValue);
			}
			else if (propertyType == typeof(Double))
			{
				var parsedValue = Double.Parse(value);
				propertyInfo.SetValue(target, parsedValue);
			}
			else if (propertyType == typeof(Decimal))
			{
				var parsedValue = Decimal.Parse(value);
				propertyInfo.SetValue(target, parsedValue);
			}
			else
			{
				var message = $"Mapping of {propertyType} is not supported, or it doesn't exist on the target object";
				throw new ArgumentException(message);
			}
		}
	}
}
