﻿using Newtonsoft.Json;
using System;

namespace TestApp
{
    public abstract class JsonEntity
    {
        public string Type { get; set; }
    }

    public abstract class Transform : JsonEntity { }

    public abstract class Shape : JsonEntity, IShape
    {
        public virtual void PrintProperties()
        {
            var props = this.GetType().GetProperties();

            Console.WriteLine(this.GetType().Name);
            Console.WriteLine("___________________");
            foreach (var p in props)
                IO.PrintPretty(p.Name, p.GetValue(this).ToString());

            Console.WriteLine(Environment.NewLine);
        }

        public abstract void Transform(Transform transform);
    }

    public abstract class CustomConverter<T> : JsonConverter<T> where T : JsonEntity { }
}
