﻿using Newtonsoft.Json;
using System;
using System.IO;
using TestApp.Shapes;

namespace TestApp
{
    public static class IO
    {
        public static ShapeWrapper DeserializeShapes()
        {
            var path = Configuration.GetShapesFilePath();
            var converter = ShapeWrapper.GetConverter();
            var json = File.ReadAllText(path);

            return JsonConvert.DeserializeObject<ShapeWrapper>(json, converter);
        }

        public static TransformWrapper DeserializeTransformations()
        {
            var path = Configuration.GetShapesFilePath();
            var converter = TransformWrapper.GetConverter();
            var json = File.ReadAllText(path);

            return JsonConvert.DeserializeObject<TransformWrapper>(json, converter);
        }

        public static void PrintPretty(string propName, string propValue)
        {
            if (propValue == null) return;

            var separation = 30 - (propName.Length + propValue.Length);
            Console.Write(propName);
            
            for (int i = 0; i < separation; i++)
                Console.Write(" ");

            Console.Write(propValue);
            Console.WriteLine();
        }
    }
}
