﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace TestApp
{
    public class RotateTransform : Transform
    {
        public int Angle { get; set; }

        public (double, double) RotatePoint(double x, double y)
        {
            var sqrt = Math.Sqrt(x * x + y * y);
            var arcTan = Math.Atan(y / x) + Angle;
            var newX = sqrt * Math.Cos(arcTan);
            var newY = sqrt * Math.Sin(arcTan);

            return (newX, newY);
        }
    }

    public class TranslateTransform : Transform
    {
        public int OffsetX { get; set; }
        public int OffsetY { get; set; }

        public int GetOffsetSum() => OffsetX + OffsetY;
    }

    public class TransformWrapper
    {
        public List<Transform> Shapes { get; set; }

        public static TransformConverter GetConverter()
        {
            return new TransformConverter();
        }
    }

    public class TransformConverter : CustomConverter<Transform>
    {
        public override Transform ReadJson(JsonReader reader, Type objectType, Transform existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            var jObject = JObject.Load(reader);

            var discriminator = DynamicMapper.GetValueFromKey(nameof(JsonEntity.Type), jObject);
            switch (discriminator)
            {
                case "rotate":
                    return DynamicMapper.CreateFromJObject<RotateTransform>(jObject);
                case "translate":
                    return DynamicMapper.CreateFromJObject<TranslateTransform>(jObject);
                default:
                    throw new NotSupportedException("No support for such transformation");
            }
        }

        public override void WriteJson(JsonWriter writer, Transform value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
