﻿using System;
using System.IO;
using System.Reflection;

namespace TestApp
{
    public static class Configuration
    {
        private const string TRANSFORMS_FILE = "transforms.json";
        private const string SHAPES_FILE = "shapes.json";


        public static string GetTransformationsFilePath() =>
            CombineWithSolutionPath(TRANSFORMS_FILE);


        public static string GetShapesFilePath() =>
            CombineWithSolutionPath(SHAPES_FILE);


        private static string CombineWithSolutionPath(string filename)
        {
            var assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
            var pathParts = AppDomain.CurrentDomain.BaseDirectory.Split(assemblyName);
            var solutionDirSplitFailed = pathParts.Length < 2;

            if (solutionDirSplitFailed)
            {
                throw new Exception("Failed to obtain solution path");
            }

            var filepath = Path.Combine(pathParts[0], filename);
            if (!File.Exists(filepath))
            {
                throw new Exception($"Failed to obtain path for file: {filename}");
            }


            return filepath;
        }
    }
}
